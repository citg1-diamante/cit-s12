package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    //Create a user
    void createUser(User user);

    //Viewing all users
    Iterable<User> getUsers();

    //Delete a user
    ResponseEntity deleteUser(Long id);
    //Update a user
    ResponseEntity updateUser(Long id, User user);
}
