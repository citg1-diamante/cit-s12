package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    //An object cannot be instantiated from interfaces.
    //@Autowired allow us to use the interface as if it was an instance of an object and allows us to use the methods from the CrudRepository

    @Autowired
    private UserRepository userRepository;

    //Create post
    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    //Get all posts
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    //Delete post
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully.", HttpStatus.OK);
    }

    //Update post
    public ResponseEntity updateUser(Long id, User user){
        //Find the post to update
        User userForUpdate = userRepository.findById(id).get();

        //Updating the title and content
        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());

        //Saving and updating a post
        userRepository.save(userForUpdate);
        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
    }
}
